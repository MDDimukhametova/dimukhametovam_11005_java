package Stack;

import Stack.Exceptions.EmptyStackException;

public class Main {
    public static void main(String[] args) throws EmptyStackException {
        Stack<Integer> stack= new Stack<Integer>();
        stack.push(1);
        stack.push(2);
        stack.push(3);

        System.out.println(stack.pop());
        System.out.println(stack.peek());

        for (Integer integer: stack) {
            System.out.println(integer);
        }
    }
}
