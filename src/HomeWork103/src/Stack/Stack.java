package Stack;

import Stack.Exceptions.EmptyStackException;

import java.util.Iterator;

public class Stack<T> implements Iterable<T> {

    @Override
    public Iterator<T> iterator() {
        return new StackIterator();
    }

    static class Node<T> {
        private T item;
        private Node<T> next;

        public Node(T item, Node<T> next) {
            this.item = item;
            this.next = next;
        }
    }

    class StackIterator implements Iterator<T> {

        Node current = head;

        @Override
        public boolean hasNext() {
            if (head == null) {
                head = current;
                return false;
            } else {
                return true;
            }
        }

        @Override
        public T next() {
            T current = head.item;
            head = head.next;
            return current;
        }
    }

    private Node<T> head;

    public Stack() {
        this.head = null;
    }

    public void push(T item) {
        Node<T> node = new Node<T>(item, head);
        head = node;
    }

    public T pop() throws EmptyStackException {
        if (head == null) {
            throw new EmptyStackException("Stack is empty!");
        } else {
            Node<T> current = head;
            head = current.next;
            return head.item;
        }
    }

    public T peek() throws EmptyStackException {
        if (head == null) {
            throw new EmptyStackException("Stack is empty!");
        } else {
            return head.item;
        }
    }
}
