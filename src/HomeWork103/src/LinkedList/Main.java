package LinkedList;

import Stack.Exceptions.EmptyStackException;

public class Main {
    public static void main(String[] args) throws EmptyStackException {
        LinkedList<Integer> linkedList = new LinkedList<Integer>();
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);

        System.out.println(linkedList.size());
        System.out.println(linkedList.get(0));


        linkedList.remove(1);

        for (Integer integer: linkedList) {
            System.out.println(integer);
        }

    }
}
