package LinkedList;

import Stack.Exceptions.EmptyStackException;

import java.util.Iterator;

public class LinkedList<T> implements Iterable<T> {

    static class Node<T> {
        T item;
        Node<T> next;

        public Node (T item, Node<T> next) {
            this.item = item;
            this.next = next;
        }
    }

    Node<T> head;

    class ListIterator implements Iterator<T> {
        Node current = head;

        @Override
        public boolean hasNext() {
            if (head == null) {
                head = current;
                return false;
            } else {
                return true;
            }
        }

        @Override
        public T next() {
            T current = head.item;
            head = head.next;
            return current;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new ListIterator() {
        };
    }
    public LinkedList() {
        this.head = null;
    }

    public void add(T item) {
        if (head == null) {
            head = new Node<T>(item, null);
        } else {
            Node<T> current = head;
            while (current.next != null) {
                current = current.next;
            }
            Node<T> newNode = new Node<T>(item, null);
            current.next = newNode;
        }
    }

    public int size() {
        if (head == null) {
            return 0;
        } else {
            int counter = 1;
            Node<T> current = head;
            while (current.next != null) {
                counter++;
                current = current.next;
            }
            return counter;
        }
    }

    public T get(int id) throws EmptyStackException {
        if (head == null) {
            throw new EmptyStackException("Stack is empty!");
        } else {
            Node<T> current = head;
            int i = 0;
            while (i != id) {
                current = current.next;
                i++;
            }
            return current.item;
        }
    }

    public void remove(int i) throws EmptyStackException {
        if (head == null) {
            throw new EmptyStackException("Stack is empty!");
        } else {
            if (i == 0) {
                head = head.next;
            } else {
                Node<T> current = head;
                int id = 0;
                while (id < i-1) {
                    current = current.next;
                    id++;
                }
                current.next = current.next.next;
            }
        }
    }


}
