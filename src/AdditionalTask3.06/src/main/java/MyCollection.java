import java.util.ArrayList;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class MyCollection <T extends Comparable<T> & Printable>{
    ArrayList<T> list = new ArrayList<>();

    public ArrayList<T> getList() {
        return list;
    }

    public void create(T item) throws IllegalArgumentException{
        for (T t : list) {
            if (t.compareTo(item) == 0) {
                throw new IllegalArgumentException();
            }
        }
        list.add(item);
    }

    public T read(int id) throws NoSuchElementException {
        if (id >= list.size()) {
            throw new NoSuchElementException();
        }
        return list.get(id);
    }

    public void update(T item, T newItem) throws NoSuchElementException {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compareTo(item) == 0) {
                list.set(i, newItem);
                return;
            }
        }
        throw new NoSuchElementException();
    }

    public void delete(T item) throws NoSuchElementException {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compareTo(item) == 0) {
                list.remove(i);
                return;
            }
        }
        throw new NoSuchElementException();
    }
}
