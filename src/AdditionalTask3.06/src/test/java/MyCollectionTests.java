import org.junit.*;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class MyCollectionTests {
    private MyCollection<TestCollection> myCollection;

    @Before
    public void prepareCollection() {
        int quantityOfItems = 3;
        myCollection = new MyCollection<>();

        for (int i = 0; i < quantityOfItems; i++) {
            myCollection.create(new TestCollection(i));
        }
    }

    @Test
    public void createTestSuccess() {
        myCollection.create(new TestCollection(3));
        assertEquals(4, myCollection.list.size());
    }

    @Test
    public void createTestFailed() {
        assertThrows(IllegalArgumentException.class, () -> myCollection.create(new TestCollection(2)));
    }

    @Test
    public void readTestSuccess() {
        assertEquals(0, myCollection.read(0).getId());
    }

    @Test
    public void readTestFailed() {
        assertThrows(NoSuchElementException.class, () -> myCollection.read(3));
    }

    @Test
    public void updateTestSuccess() {
        myCollection.update(new TestCollection(2), new TestCollection(6));
        assertEquals(6, myCollection.list.get(2).getId());
    }

    @Test
    public void updateTestFailed() {
        assertThrows(NoSuchElementException.class,
                () -> myCollection.update(new TestCollection(6), new TestCollection(7)));
    }

    @Test
    public void deleteTestSuccess() {
        myCollection.delete(new TestCollection(1));
        assertEquals(2, myCollection.list.size());
    }

    @Test
    public void deleteTestFailed() {
        assertThrows(NoSuchElementException.class, () -> myCollection.delete(new TestCollection(6)));
    }
}
