public class TestCollection implements Comparable<TestCollection>, Printable{
    private int id;

    public int getId() {
        return id;
    }

    public TestCollection(int id) {
        this.id = id;
    }

    public void print() {
        System.out.println("TestCollection: " + id);
    }

    public int compareTo(TestCollection o) {
        return Integer.compare(id, o.id);
    }
}
