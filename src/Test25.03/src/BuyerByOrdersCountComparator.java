import java.util.Comparator;

public class BuyerByOrdersCountComparator implements Comparator<Buyer> {
    @Override
    public int compare(Buyer o1, Buyer o2) {
        Integer i1 = 0;
        for (int i = 0; i < o1.getCities().size(); i++) {
            i1 += o1.getCities().get(i).getOrders().size();
        }

        Integer i2 = 0;
        for (int i = 0; i < o2.getCities().size(); i++) {
            i2 += o2.getCities().get(i).getOrders().size();
        }
        return i1.compareTo(i2);
    }
}
