import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        Reader reader = new Reader();
        ArrayList<Buyer> buyers = new ArrayList<>();
        ArrayList<String> stringArrayList = reader.readStr();
        ArrayList<String[]> strings = reader.divideStr(stringArrayList);
        reader.distribute(buyers, strings);

        BuyerByNameComparator nameComparator = new BuyerByNameComparator();
        BuyerByCityCountComparator cityCountComparator = new BuyerByCityCountComparator();
        BuyerByOrdersCountComparator ordersCountComparator = new BuyerByOrdersCountComparator();

        Comparator<Buyer> first = nameComparator.thenComparing(cityCountComparator).thenComparing(ordersCountComparator);
        Comparator<Buyer> second = ordersCountComparator.thenComparing(cityCountComparator).thenComparing(nameComparator);

        TreeSet<Buyer> firstSet = new TreeSet<>(first);
        TreeSet<Buyer> secondSet = new TreeSet<>(second);

        firstSet.addAll(buyers);
        secondSet.addAll(buyers);
    }
}
