import java.util.Comparator;

public class BuyerByCityCountComparator implements Comparator<Buyer> {
    @Override
    public int compare(Buyer o1, Buyer o2) {
        Integer i1 = o1.getCities().size();
        Integer i2 = o2.getCities().size();
        return i1.compareTo(i2);
    }
}
