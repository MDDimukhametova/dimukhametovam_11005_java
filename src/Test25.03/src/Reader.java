import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Reader {

    public ArrayList<String> readStr() {
        ArrayList<String> strings = new ArrayList<>();
        Scanner sc = null;
        try {
            sc = new Scanner(new FileReader("data/orders.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (sc.hasNext()) {
            String str = sc.next();
            strings.add(str);
        }
        return strings;
    }

    public ArrayList<String[]> divideStr (ArrayList<String> stringsArray) {
        ArrayList<String[]> strArList = new ArrayList<>();
        for (String str: stringsArray) {
            String[] strAr = str.split("\\|");
            strArList.add(strAr);
        }
        return strArList;
    }

    public void distribute (ArrayList<Buyer> buyers, ArrayList<String[]> strAr) {
        if (buyers.size() == 0) {
            createNewBuyer(buyers, strAr);
        }
        for (Buyer buyer : buyers) {
            for (int i = 0; i < strAr.size(); i++) {
                if (buyer.getName().equals(strAr.get(i)[0])) {
                    for (City city : buyer.getCities()) {
                        if (city.getName().equals(strAr.get(i)[1])) {
                            for (Order order : city.getOrders()) {
                                if (order.getName().equals(strAr.get(i)[2])) {
                                    order.setNumber(order.getNumber() + Integer.parseInt(strAr.get(i)[3]));
                                } else {
                                    Order o = new Order(strAr.get(i)[2], Integer.parseInt(strAr.get(i)[3]));
                                    city.getOrders().add(o);
                                }
                            }
                        } else {
                            ArrayList<Order> orders = new ArrayList<>();
                            int numberOfOrders = Integer.parseInt(strAr.get(i)[3]);
                            orders.add(new Order(strAr.get(i)[2], numberOfOrders));
                            buyer.getCities().add(new City(strAr.get(i)[1], orders));
                        }
                    }
                } else {
                    createNewBuyer(buyers, strAr);
                }
            }
        }
    }

    private void createNewBuyer(ArrayList<Buyer> buyers, ArrayList<String[]> strAr) {
        for (int i = 0; i < strAr.size(); i++) {
            ArrayList<City> cityArrayList = new ArrayList<>();
            ArrayList<Order> orders = new ArrayList<>();
            if (tryParse(strAr.get(i)[3])) {
                int n = Integer.parseInt(strAr.get(i)[3]);
                orders.add(new Order(strAr.get(i)[2], n));
                cityArrayList.add(new City(strAr.get(i)[1], orders));
                Buyer b = new Buyer(strAr.get(i)[0], cityArrayList);
                buyers.add(b);
            }
        }
    }

    private boolean tryParse (String s) {
        try {
            Integer.parseInt(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}

