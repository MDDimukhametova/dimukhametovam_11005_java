import Exceptions.NonExistentProductException;
import org.junit.*;

import static org.junit.Assert.*;

public class StorageTests {
    private Storage storage;
    private ProductsRuntimeSaver saver;

    @Before
    public void prepareProducts() {
        saver = new ProductsRuntimeSaver();
        saver.getProducts().add(new Product("apple", 10));
        saver.getProducts().add(new Product("orange", 15));
        saver.getProducts().add(new Product("lemon", 20));
        storage = new Storage(saver);
    }

    @Test
    public void addProductTest() {
        storage.createNewProduct("pineapple", 25);

        assertEquals(4, saver.getProducts().size());
        assertEquals("pineapple", saver.getProducts().get(3).getName());
        assertEquals(25, saver.getProducts().get(3).getCount());
    }

    @Test
    public void getProductTestSuccess() {
        Product product = storage.getProduct("lemon");
        assertEquals("lemon", product.getName());
        assertEquals(20, product.getCount());
        assertNotNull(product);
    }

    @Test
    public void getProductTestFailed() {
        Product product = storage.getProduct("pineapple");
        assertNull(product);
    }

    @Test
    public void deleteProductTestSuccess() throws NonExistentProductException {
        storage.deleteProduct("orange");
        assertEquals(2, saver.getProducts().size());
    }

    @Test
    public void deleteNonExistentProductTest() {
        Product product = new Product("pineapple", 25);
        assertThrows(NonExistentProductException.class, () -> storage.deleteProduct(product.getName()));
    }

    @Test
    public void changeQuantityOfProductTestSuccess() throws NonExistentProductException {
        Product product = storage.getProduct("apple");
        storage.changeQuantityOfProduct(product.getName(), 30);
        assertEquals(30, storage.getProduct(product.getName()).getCount());
    }

    @Test
    public void changeQuantityOfNonExistentProductTest() {
        Product product = new Product("pineapple", 25);
        assertThrows(NonExistentProductException.class, () -> storage.changeQuantityOfProduct(product.getName(), 35));
    }
}
