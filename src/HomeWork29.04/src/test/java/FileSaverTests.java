import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.time.temporal.TemporalAccessor;
import java.util.*;

import static org.junit.Assert.*;

public class FileSaverTests {
    private ProductsFileSaver saver;
    private ArrayList<Product> products;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void openFile() throws IOException {
        File file = folder.newFile();
        saver = new ProductsFileSaver(file);

        products = new ArrayList<>();
        products.add(new Product("apple", 10));
        products.add(new Product("orange", 15));
        products.add(new Product("lemon", 20));
    }

    @Test
    public void readWriteProductsTest() {
        saver.saveProducts(products);
        ArrayList<Product> current = saver.getProducts();

        assertEquals(products.size(), current.size());

        for (int i = 0; i < products.size(); i++) {
            assertEquals(products.get(i).getName(), current.get(i).getName());
            assertEquals(products.get(i).getCount(), current.get(i).getCount());
        }
    }
}
