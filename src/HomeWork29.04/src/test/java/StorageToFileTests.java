import org.junit.*;

import java.util.*;

import static org.junit.Assert.*;

public class StorageToFileTests {
    private Storage storage;
    private ArrayList<Product> products;

    @Before
    public void prepareProducts() {
        products = new ArrayList<>();
        products.add(new Product("apple", 10));
        products.add(new Product("orange", 15));
        products.add(new Product("lemon", 20));

        ProductsFileSaver saver = new ProductsFileSaver("data/file.txt");
        saver.saveProducts(products);

        storage = new Storage(saver);
    }

    @Test
    public void readToStorageTest() {
        assertEquals(products.size(), storage.getProducts().size());

        for (int i = 0; i < products.size(); i++) {
            assertEquals(products.get(i).getName(), storage.getProducts().get(i).getName());
            assertEquals(products.get(i).getCount(), storage.getProducts().get(i).getCount());
        }
    }

    @Test
    public void createProductTest() {
        products.add(new Product("pineapple", 25));
        storage.createNewProduct("pineapple", 25);

        storage.getSaver().saveProducts(storage.getProducts());

        ArrayList<Product> current = storage.getSaver().getProducts();

        assertEquals(products.size(), current.size());

        for (int i = 0; i < products.size(); i++) {
            assertEquals(products.get(i).getName(), current.get(i).getName());
            assertEquals(products.get(i).getCount(), current.get(i).getCount());
        }
    }
}
