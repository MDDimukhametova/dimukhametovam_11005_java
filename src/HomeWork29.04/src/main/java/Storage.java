import Exceptions.NonExistentProductException;

import java.util.ArrayList;

public class Storage {
    private ArrayList<Product> products;
    private ProductsSaver saver;
    private ArrayList<ProductsListener> listeners;

    public Storage(ProductsSaver saver) {
        this.saver = saver;
        this.products = saver.getProducts();
        this.listeners = new ArrayList<>();
    }

    public ProductsSaver getSaver() {
        return saver;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void addListener(ProductsListener listener) {
        listeners.add(listener);
    }

    public void printAllProducts() {
        for (Product product: products) {
            System.out.println(product.getName() + ": " + product.getCount());
        }
    }

    public void createNewProduct(String name, int count) {
        Product product = new Product(name, count);
        products.add(product);
        for (ProductsListener pl: listeners) {
            pl.productsUpdated();
        }
    }

    public void deleteProduct(String name) throws NonExistentProductException {
        Product product = getProduct(name);
        if (product != null && products.contains(product)) {
            products.remove(product);
            saver.saveProducts(products);
            for (ProductsListener pl: listeners) {
                pl.productsUpdated();
            }
        } else throw new NonExistentProductException("This product is not on the list!");
    }

    public void changeQuantityOfProduct (String name, int newCount) throws NonExistentProductException {
        Product product = getProduct(name);
        if (product != null && products.contains(product)) {
            product.setCount(newCount);
            for (ProductsListener pl: listeners) {
                pl.productsUpdated();
            }
        } else throw new NonExistentProductException("This product is not on the list!");

    }

    public Product getProduct(String name) {
        for (Product product: products) {
            if (product.getName().equals(name)) {
                return product;
            }
        }
        return null;
    }
}
