package Exceptions;

public class NonExistentProductException extends Exception{
    public NonExistentProductException(String message) {
        super(message);
    }
}
