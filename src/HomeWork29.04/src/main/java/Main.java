import Exceptions.NonExistentProductException;

public class Main {
    public static void main(String[] args) throws NonExistentProductException {
        ProductsFileSaver saver = new ProductsFileSaver("data/file.txt");
        Storage storage = new Storage(saver);

        storage.createNewProduct("apple", 10);
        storage.createNewProduct("orange", 15);
        storage.createNewProduct("pineapple", 20);

        storage.addListener(storage::printAllProducts);

        storage.addListener(() -> {
            int unavailable = 0;
            for (Product product: storage.getProducts()) {
                unavailable += product.getCount();
            }
            System.out.println("Unavailable: " + unavailable);
        });

        storage.addListener(() -> {
            int max = 0;
            for (Product product: storage.getProducts()) {
                if (product.getCount() > max) {
                    max = product.getCount();
                }
            }
            System.out.println("Max: " + max);
        });

        storage.changeQuantityOfProduct("orange", 30);
        storage.deleteProduct("pineapple");

        storage.printAllProducts();
    }
}
