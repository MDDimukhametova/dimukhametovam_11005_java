import java.util.ArrayList;

public class ProductsRuntimeSaver implements ProductsSaver{
    private ArrayList<Product> products;

    public ProductsRuntimeSaver() {
        products = new ArrayList<>();
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void saveProducts(ArrayList<Product> products) {
        this.products = products;
    }
}
