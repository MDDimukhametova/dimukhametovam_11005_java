import java.io.*;
import java.util.*;

public class ProductsFileSaver implements ProductsSaver{
    private File file;

    public ProductsFileSaver(File file) {
        this.file = file;
    }

    public ProductsFileSaver(String fileName) {
        file = new File(fileName);
    }

    @Override
    public ArrayList<Product> getProducts() {
        ArrayList<Product> list = new ArrayList<>();

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            Product[] productsToRead = (Product[]) objectInputStream.readObject();

            list.addAll(Arrays.asList(productsToRead));
        }
        catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return list;
    }

    @Override
    public void saveProducts(ArrayList<Product> products) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file))) {
            Product[] current = new Product[products.size()];
            for (int i = 0; i < products.size(); i++) {
                current[i] = products.get(i);
            }
            objectOutputStream.writeObject(current);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
