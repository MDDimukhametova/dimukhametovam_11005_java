package BracketChecker;

import java.util.Scanner;
import java.util.Stack;

public class Algorithm {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Stack<Character> stack = new Stack<>();
        char[] starter = new char[]{'(', '[', '{'};
        char[] finisher = new char[]{')', ']', '}'};
        while (true) {
            String str = sc.nextLine();
            boolean error = false;
            char[] string = str.toCharArray();
            for (int i = 0; i < string.length; i++) {
                Character c = string[i];

                for (int j = 0; j < starter.length; j++) {
                    if (c.equals(starter[j])) {
                        stack.push(c);
                    }
                }

                for (int j = 0; j < finisher.length; j++) {
                    if (!stack.isEmpty()) {
                        if (c.equals(finisher[j])) {
                            Character temp = stack.peek();
                            if (temp.equals(starter[j])) {
                                stack.pop();
                            }
                        }
                    }
                }
            }

            if (!stack.isEmpty()) {
                error = true;
            }

            if (error) {
                System.out.println("String is incorrect! Please, enter the string again!");
            } else {
                System.out.println("String is correct!");
            }
        }
    }
}
