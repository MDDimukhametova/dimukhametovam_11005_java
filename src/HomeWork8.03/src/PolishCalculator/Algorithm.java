package PolishCalculator;

import java.util.Scanner;
import java.util.Stack;

public class Algorithm {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Stack<Integer> stack = new Stack<>();

        boolean error = false;
        String str = sc.nextLine();
        String[] strArray = str.split(" ");
        int strArrayPointer = strArray.length;

        for (int i = 0; i < strArray.length; i++) {
            if (tryParseInt(strArray[i])) {
                int x = Integer.parseInt(strArray[i]);
                stack.push(x);
            } else {
                if (stack.size() >= 2) {
                    if (strArray.length > 3 && strArrayPointer <= stack.size() * 2 ) {
                        int b = stack.pop();
                        int a = stack.pop();
                        stack.push(calculate(a, b, strArray[i].charAt(0)));
                        strArrayPointer -= 2;
                    } else error = true;
                } else {
                    error = true;
                }
            }
        }
        if (!error) {
            System.out.println(stack.pop());
        } else {
            System.out.println("Incorrect expression! Please, enter the expression again!");
        }
    }

    public static boolean tryParseInt(String s) {
        try {
            Integer.parseInt(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static int calculate(int item1, int item2, char operator) {
        switch (operator) {
            case '+':
                return item1 + item2;
            case '-':
                return item1 - item2;
            case '*':
                return item1 * item2;
            case '/':
                return item1 / item2;
            default:
                throw new IllegalStateException("Unexpected value: " + operator);
        }
    }
}

