import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<String, Integer> map = createOrdersFromFile();

        while (true) {

            System.out.println( "\n" +
                    "Choose the function:" + "\n" +
                    "1)  print all products" + "\n" +
                    "2)  add new product to the list" + "\n" +
                    "3)  remove product from the list" + "\n" +
                    "4)  change the quantity of the product" + "\n");

            int function = sc.nextInt();
            switch (function) {
                case 1:
                    for (Map.Entry<String, Integer> entry : map.entrySet()) {
                        System.out.println(entry.getKey() + " " + entry.getValue());
                    }
                    break;
                case 2:
                    String product = sc.next();
                    int quantity = sc.nextInt();

                    if (map.containsKey(product)) {
                        map.put(product, map.get(product) + quantity);
                    } else {
                        map.put(product, quantity);
                    }

                    try {
                        FileWriter fileWriter = new FileWriter("data/file.txt", true);
                        fileWriter.write(product + " " + quantity);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    String productToRemove = sc.next();
                    for (int i = 0; i < map.size(); i++) {
                        if (map.containsKey(productToRemove)) {
                            map.remove(productToRemove);
                        }
                    }
                    break;
                case 4:
                    String currentProduct = sc.next();
                    int newQuantity = sc.nextInt();
                    if (map.containsKey(currentProduct)) {
                        map.put(currentProduct, newQuantity);
                    } else System.out.println("There is no such product in the list!");
                    break;
                default:
                    System.out.println("Please, choose the function again!");
                    break;
            }
        }
    }

    private static Map<String, Integer> createOrdersFromFile () {
        Map<String, Integer> map = new HashMap<>();
        try {
            Scanner sc = new Scanner(new FileReader("data/file.txt"));
            while (sc.hasNext()) {
                String product = sc.next();
                int quantity = sc.nextInt();

                if (map.containsKey(product)) {
                    map.put(product, map.get(product) + quantity);
                } else {
                    map.put(product, quantity);
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return map;
    }
}
