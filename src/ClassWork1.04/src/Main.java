import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<String, Map<String, Map<String, Integer>>> map = new HashMap<>();
        String string = "";

        try {
            Scanner sc = new Scanner(new FileReader("data/orders.txt"));

            while (sc.hasNext()) {
                string = sc.nextLine() + "|";
                String[] words = string.split("\\|");

                if (map.containsKey(words[0])) {
                    if (map.get(words[0]).containsKey(words[1])) {
                        if (map.get(words[0]).get(words[1]).containsKey(words[2])) {
                            map.get(words[0]).get(words[1]).put(words[2], (map.get(words[0]).get(words[1]).get(words[2]) + Integer.parseInt(words[3])));
                        } else {
                            map.get(words[0]).get(words[1]).put(words[2], Integer.parseInt(words[3]));
                        }
                    } else {
                        Map <String, Map<String, Integer>> currentCity = new HashMap<>();
                        Map<String, Integer> currentOrder = new HashMap<>();

                        currentOrder.put(words[2], Integer.parseInt(words[3]));
                        currentCity.put(words[1], currentOrder);
                    }
                } else {
                    Map<String, Integer> current = new HashMap<>();
                    current.put(words[2], Integer.parseInt(words[3]));
                    Map<String, Map<String, Integer>> map2= new HashMap<>();
                    map2.put(words[1], current);
                    map.put(words[0], map2);
                }
            }
            printTripleMap(map);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static void printTripleMap (Map <String, Map <String, Map<String, Integer>>> map) {
        for (Map.Entry<String, Map<String, Map<String, Integer>>> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":");
            for (Map.Entry<String, Map<String, Integer>> entry1 : entry.getValue().entrySet()) {
                System.out.println(entry1.getKey() + ":");
                for (Map.Entry<String, Integer> entry2 : entry1.getValue().entrySet())
                    System.out.println("\t" + "\t" + entry2.getKey() + " - " + entry2.getValue());
            }
        }
    }
}
