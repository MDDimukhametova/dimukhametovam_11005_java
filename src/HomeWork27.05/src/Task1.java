import java.util.Scanner;

public class Task1 {
    static volatile int sum = 1;
    static volatile int iterator = 1;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (iterator < n)
                    synchronized (this) {
                        iterator++;
                        sum *= iterator;
                        System.out.println(Thread.currentThread().getName() + " " + sum);
                        try {
                            Thread.sleep(1500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (iterator < n) {
                    synchronized (this) {
                        iterator++;
                        sum *= iterator;
                        System.out.println(Thread.currentThread().getName() + " " + sum);
                        try {
                            Thread.sleep(1500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (iterator < n) {
                    synchronized (this) {
                        sum *= iterator;
                        System.out.println(Thread.currentThread().getName() + " " + sum);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(sum);
    }
}
