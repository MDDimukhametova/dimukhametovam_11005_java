import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        ArrayList<String> allWords = new ArrayList<>();
        ArrayList<String> even = new ArrayList<>();
        ArrayList<String> odd = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new FileReader("data/text.txt"));
            while (scanner.hasNext()) {
                allWords.add(scanner.next().toLowerCase(Locale.ROOT));
            }

            Thread t1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (this) {
                        allWords.stream().filter(x -> x.chars().distinct().count() % 2 != 0)
                                .forEach(x -> {
                                    odd.add(x);
                                    System.out.println(Thread.currentThread().getName() + "added odd" );
                                });
                    }
                }
            });

            Thread t2 = new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (this) {
                        allWords.stream().filter(x -> x.chars().distinct().count() % 2 != 0)
                                .forEach(x -> {
                                    even.add(x);
                                    System.out.println(Thread.currentThread().getName() + "added even");
                                });
                    }
                }
            });

            t1.start();
            t2.start();
            t1.join();
            t2.join();
        } catch (FileNotFoundException | InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(even);
        System.out.println(odd);
    }
}
