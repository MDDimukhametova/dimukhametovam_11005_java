import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        task2("data/task2.txt");
    }

    private static void task1(String path) {
        Map<String, Map<String, Integer>> map = new HashMap<>();
        try {
            Stream<String> stream = Files.lines(Paths.get(path));
            var var = stream.map(s -> s.split("\\|"))
                    .map(s -> {
                        if (map.containsKey(s[0])) {
                            if (map.get(s[0]).containsKey(s[1])) {
                                map.get(s[0]).put(s[1], map.get(s[0]).get(s[1]) + Integer.parseInt(s[2]));
                            } else map.get(s[0]).put(s[1], Integer.parseInt(s[2]));
                        } else {
                            Map<String, Integer> map1 = new HashMap<>();
                            map1.put(s[1], Integer.parseInt(s[2]));
                            map.put(s[0], map1);
                        }
                       return map;
                    }).
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void task2 (String path) {
        Map<String, Integer> map = new HashMap<>();
        try {
            Stream<String> stream = Files.lines(Paths.get(path));
            stream.map(s -> s.split("\\|")).map(s -> {
                if (map.containsKey(s[0])) {
                    map.put(s[0], Integer.parseInt(s[1]) + map.get(s[0]));
                } else map.put(s[0], Integer.parseInt(s[1]));
                return map;
            }).filter(currentMap -> {
                int x = 0;
                for (Integer i : map.values()) {
                    x += i;
                }
                x = x/map.values().size();
                for (Integer i : map.values()) {
                    if (i > x) return true;
                }
                return false;
            }).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
