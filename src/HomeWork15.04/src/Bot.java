import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;

public class Bot {
    private HashMap<String, Method> commands;

    public Bot() {
        commands = new HashMap<>();

        for (Method m: this.getClass().getDeclaredMethods()) {
            if (!m.isAnnotationPresent(Command.class)) continue;

            Command cmd = m.getAnnotation(Command.class);
            for (String name : cmd.aliases()) {
                commands.put(name, m);
            }
        }
    }

    public String processUserInput(String string) {
        if (string.isEmpty()) return "Moя твоя не понимать!";

        String[] splitted = string.split(" ");
        String command = splitted[0].toLowerCase();
        String[] args = Arrays.copyOfRange(splitted, 1, splitted.length);

        Method m = commands.get(command);

        if (m == null) {
            return "Моя твоя не понимать!";
        }

        try {
            return (String) m.invoke(this, (Object) args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            return "Что-то пошло не так, попробуйте еще раз!";
        }
    }

    @Command(aliases = {"привет", "bonjoir", "hello"},
            args = "",
            description = "Будь культурным, поздоровайся!",
            id = 0,
            inProgress = false)
    public static String hello(String[] args) {
        return "Привет!";
    }

    @Command(aliases = {"sum", "сумма", "сложи"},
            args = {"первое число", "второе число"},
            description = "Складываю числа!",
            id = 1,
            inProgress = false)
    public String sum(String[] args) {
        int sum = Integer.parseInt(args[0]) + Integer.parseInt(args[1]);
        return Integer.toString(sum);
    }

    @Command(aliases = {"multiplication", "умножение", "произведение", "умножь"},
            args = {"первое число", "второе число"},
            description = "Умножаю числа!",
            id = 2,
            inProgress = false)
    public String multiplication(String[] args) {
        int mul = Integer.parseInt(args[0]) * Integer.parseInt(args[1]);
        return Integer.toString(mul);
    }

    @Command(aliases = {"dividing", "деление", "раздели"},
            args = {"первое число", "второе число"},
            description = "Делю числа!",
            id = 3,
            inProgress = false)
    public String dividing(String[] args) {
        int div = Integer.parseInt(args[0]) / Integer.parseInt(args[1]);
        return Integer.toString(div);
    }

    @Command(aliases = {"help", "помощь", "помоги"},
            args = "",
            description = "Вывожу список команд!",
            id = 4,
            inProgress = false)
    public String help(String[] args) {
        StringBuilder builder = new StringBuilder("Я умею в такие команды: \n");

        for (Method m : this.getClass().getDeclaredMethods()) {
            if (!m.isAnnotationPresent(Command.class)) {
                continue;
            }

            Command cmd = m.getAnnotation(Command.class);

            if (args.length > 0) {
                for (String alias : cmd.aliases()) {
                    if (alias.equals(args[0])) {
                        builder = new StringBuilder("Информация о команде" + cmd.aliases()[0] + ": ");
                        builder.append("\n").append(cmd.description());
                        if (cmd.args().length > 0) {
                            String str = Arrays.toString(cmd.args());
                            str = str.replaceAll("[\\[\\]]", "");
                            if (!str.isEmpty())
                                builder.append("\n").append("Аргументы: ").append(str);
                        }
                        return builder.toString();
                    }
                    builder.append(Arrays.toString(cmd.aliases())).append(": ").append(cmd.description());
                }
            }
        }
        return builder.toString();
    }
}
