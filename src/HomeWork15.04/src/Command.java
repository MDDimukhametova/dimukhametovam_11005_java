import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Command {
    String[] aliases();
    String[] args();
    String description();
    int id();
    boolean inProgress();

}
