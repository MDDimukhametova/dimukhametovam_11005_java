import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Bot bot = new Bot();

        while (true) {
            String message = sc.nextLine();
            if (message.equals("exit")) {
                break;
            }

            String answer = bot.processUserInput(message);
            System.out.println(answer);
        }
    }
}
