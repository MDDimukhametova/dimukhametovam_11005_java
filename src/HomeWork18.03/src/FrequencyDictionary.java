import java.io.*;
import java.util.*;

public class FrequencyDictionary {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        try {
            Scanner sc = new Scanner(new FileReader("data/frequencyDictionary.txt"));
            String string = "";
            while (sc.hasNext()) {
                string = string + " " + sc.next();
            }
            String[] words = string.trim().replaceAll("\\p{Punct}", " ").toLowerCase().split("\\s+");

            for (String key: words) {
                if (map.containsKey(key)) {
                    map.put(key, map.get(key) + 1);
                } else {
                    map.put(key, 1);
                }
            }

            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                System.out.println(entry.getKey() + " " + entry.getValue());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
