import java.io.*;
import java.util.*;

public class AccountingForPurchases {
    public static void main(String[] args) {

        Map<String, Map<String, Integer>> map = new HashMap<>();

        try {
            Scanner sc = new Scanner(new FileReader("data/purchases.txt"));
            while (sc.hasNext()) {
                String name = sc.next();
                String product = sc.next();
                int quantity = sc.nextInt();

                if (map.containsKey(name)) {
                    if (map.get(name).containsKey(product)) {
                        map.get(name).put(product, map.get(name).get(product) + quantity);
                    } else {
                        map.get(name).put(product, quantity);
                    }
                } else {
                    Map<String, Integer> current = new HashMap<>();
                    current.put(product, quantity);
                    map.put(name, current);
                }
            }
            printDoubleMap(map);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void printDoubleMap (Map<String, Map<String, Integer>> map) {
        for (Map.Entry<String, Map<String, Integer>> entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":");
            for (Map.Entry<String, Integer> entry1 : entry.getValue().entrySet())
                System.out.println("\t" + entry1.getKey() + " - " + entry1.getValue());
        }
    }

}
